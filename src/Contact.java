
import java.util.*;

public class Contact {

    static Scanner kb = new Scanner(System.in);
    // for login panel
    static String recieveUser;
    static String recievePass;
    static boolean foundID = false, foundPass = false;
    // for create list
    static ArrayList<User> arr = new ArrayList<>();
    // for edit panel
    static User wait;
    static boolean foundEditPass = false;
    // for checkAdd panel
    static String addUser, addPass, addFname, addLname, editUser, editPass, editFname, editLname;
    static double addWeight, addHeight, editWeight, editHeight;

    public static void main(String[] args) {
        load();
        sayLogin();
    }

    public static void load() {
        arr.add(new User("admin", "1234", "kevin", "kyle", 178, 75));
        arr.add(new User("james", "4567", "clint", "eastwood", 178, 75));
    }
    // for message dialog

    public static void showErrorLoginUser() {
        System.err.println("User is incorrect.");
    }

    public static void showErrorLoginPass() {
        System.err.println("Password is incorrect.");
    }

    public static void showSuccess() {
        System.out.println("Success.");
    }

    public static void showPleaseChoose() {
        System.out.print("Please choose : ");
    }

    public static void showErrorInput() {
        System.err.println("Input didn't match.");
    }

    public static void showErrorAddUser() {
        System.err.println("If Username has been exist. This message will show.");
    }

    public static void showErrorZero() {
        System.err.println("Cannot add or edit with a zero");
    }

    public static void showErrorBlank() {
        System.err.println("Cannot add or edit with a blank");
    }

    public static void showErrorField() {
        System.err.println("Some field didn't fill.");
    }

    public static void showErrorIndexOfTable() {
        System.err.println("This list not have this index");
    }

    public static void showErrorWeight() {
        System.err.println("Weight must be number.");
    }

    public static void showErrorHeight() {
        System.err.println("Height must be number.");
    }

    public static void showErrorWeightMinimum() {
        System.err.println("Weight must more than zero.");
    }

    public static void showErrorHeightMinimum() {
        System.err.println("Height must more than zero.");
    }

    public static void showErrorEditPass() {
        System.err.println("password didn't match");
    }

    // for Login menu
    public static void sayLogin() {
        System.out.println("Please input your username and password");
        System.out.print("Username : ");
        recieveUser = kb.next();
        System.out.print("Password : ");
        recievePass = kb.next();
        checkLogin(recieveUser, recievePass);
    }

    public static void checkLogin(String user, String password) {
        for (User a : arr) {
            if (recieveUser.equals(a.user)) {
                foundID = true;
                if (recievePass.equals(a.getPass())) {
                    foundPass = true;
                    break;
                } else {
                    foundPass = false;
                    break;
                }
            } else {
                foundID = false;
            }
        }
        checkLoginFound();
    }

    public static void checkLoginFound() {
        if (foundID == true && foundPass == true) {
            showSuccess();
            sayMenu();
        } else if (foundID == true && foundPass == false) {
            showErrorLoginPass();
            sayLogin();
        } else {
            showErrorLoginUser();
            sayLogin();
        }
    }

    // for menu panel
    public static void sayMenu() {
        System.out.println("Menu Panel");
        System.out.println("1. Add");
        System.out.println("2. Show");
        System.out.println("3. Edit");
        System.out.println("4. Log Out");
        System.out.println("5. Exit");
        System.out.println("");
        checkMenu();
    }

    public static void checkMenu() {
        showPleaseChoose();
        String input = kb.next();
        switch (input) {
            case "1":
                sayAdd();
                break;
            case "2":
                sayShow();
                break;
            case "3":
                sayCheckPass();
                break;
            case "4":
                logOut();
                break;
            case "5":
                System.exit(0);
                break;
            default:
                showErrorInput();
                sayMenu();
        }
    }

    // for checkAdd panel
    public static void sayAdd() {
        System.out.println("Please input information");
        System.out.println("1. Username 2. Password 3. Firstname 4. Lastname 5. Weight 6. Height");
        System.out.println("(0) back");
        checkAdd();
    }

    public static void chooseAddUser() {
        System.out.print("username : ");
        addUser = kb.next();
        if (addUser.equals("0")) {
            sayMenu();
        } else {
            for (User a : arr) {
                if (a.getUser().equals(addUser)) {
                    showErrorAddUser();
                    chooseAddUser();
                }
            }
            System.out.println("Username can use.");
        }

    }

    public static void chooseAddWeight() {
        boolean check = false;
        while (!check) {
            try {
                System.out.print("weight (cannot be zero) : ");
                addWeight = kb.nextDouble();
                if (addWeight == 0) {
                    sayMenu();
                    break;
                } else if (addWeight < 0) {
                    showErrorWeightMinimum();
                    chooseAddWeight();
                    break;
                }
                check = true;
            } catch (InputMismatchException e) {
                showErrorWeight();
                kb.next();
            }
        }
        System.out.println("Weight can use.");
    }

    public static void chooseAddHeight() {
        boolean check = false;
        while (!check) {
            try {
                System.out.print("height (cannot be zero) : ");
                addHeight = kb.nextDouble();
                if (addHeight == 0) {
                    sayMenu();
                    break;
                }
                else if (addHeight < 0) {
                    showErrorHeightMinimum();
                    chooseAddHeight();
                    break;
                }
                check = true;
            } catch (InputMismatchException e) {
                showErrorHeight();
                kb.next();
            }
        }
        System.out.println("Height can use.");
    }

    public static void chooseAddPass() {
        System.out.print("password : ");
        addPass = kb.next();
        if (addPass.equals("0")) {
            sayMenu();
        }
    }

    public static void chooseAddFname() {
        System.out.print("firstname : ");
        addFname = kb.next();
        if (addFname.equals("0")) {
            sayMenu();
        }
    }

    public static void chooseAddLname() {
        System.out.print("lastname : ");
        addLname = kb.next();
        if (addLname.equals("0")) {
            sayMenu();
        }
    }

    public static void checkAdd() {
        chooseAddUser();
        chooseAddPass();
        chooseAddFname();
        chooseAddLname();
        chooseAddWeight();
        chooseAddHeight();
        checkAddSave();
    }

    public static void checkAddSave() {
        System.out.println("(0) back / (Y) save");
        showPleaseChoose();
        String input = kb.next();
        if (input.equalsIgnoreCase("Y")) {
            arr.add(new User(addUser, addPass, addFname, addLname, addWeight, addHeight));
            showSuccess();
            sayMenu();
        } else if (input.equals("0")) {
            sayAdd();
        } else {
            checkAddSave();
        }

    }

    public static void checkAddDefault() {
        addUser = "";
        addPass = "";
        addFname = "";
        addLname = "";
        addWeight = 0;
        addHeight = 0;
    }

    // for show panel
    public static void sayShow() {
        System.out.println("Show Panel");
        for (int i = 0; i < arr.size(); i++) {
            System.out.println(i + 1 + ". " + arr.get(i).user);
        }
        System.out.println("(0) back");
        show();
    }

    public static void checkShow(int input) {
        for (int i = 0; i < arr.size(); i++) {
            if (input == i + 1) {
                System.out.println(arr.get(i));
                sayShow();
                break;
            } else if (input == 0) {
                sayMenu();
                break;
            } else if (input > arr.size()) {
                showErrorInput();
                sayShow();
                break;
            }
        }
        showErrorInput();
        sayShow();
    }

    public static void show() {
        boolean check = false;
        int input;
        while (!check) {
            try {
                showPleaseChoose();
                input = kb.nextInt();
                checkShow(input);

            } catch (InputMismatchException e) {
                showErrorIndexOfTable();
                kb.next();
            }
        }
    }

    // for edit panel
    public static void sayCheckPass() {
        showCheckPass();
        String input = kb.next();
        if (input.equals("0")) {
            sayMenu();
        } else {
            checkPassFound(input);
        }
    }

    public static void showCheckPass() {
        System.out.println("Please input your password");
        System.out.println("(0) back");
        System.out.print("Password : ");
    }

    public static void checkPassFound(String input) {
        for (User a : arr) {
            if (input.equals(a.getPass()) && input.equals(recievePass)) {
                wait = new User(a.getUser(), a.getPass(), a.getF(), a.getL(), a.getW(), a.getH());
                sayEdit();
            }
        }
        if (foundEditPass == false) {
            showErrorEditPass();
            sayCheckPass();
        }
    }

    public static void sayEdit() {
        showEdit();
        String input = kb.next();
        if (input.equals("0")) {
            sayMenu();
        } else if (input.equals("2")) {
            chooseEditPass();
            sayEdit();
        } else if (input.equals("3")) {
            chooseEditFname();
            sayEdit();
        } else if (input.equals("4")) {
            chooseEditLname();
            sayEdit();
        } else if (input.equals("5")) {
            chooseEditWeight();
            sayEdit();
        } else if (input.equals("6")) {
            chooseEditHeight();
            sayEdit();
        } else if (input.equalsIgnoreCase("Y")) {
            checkEditSave();
        } else {
            showErrorInput();
            sayEdit();
        }

    }

    public static void showEdit() {
        System.out.println("Edit Panel");
        System.out.println(
                "1. Username: " + wait.getUser() + " 2. Password: " + wait.getPass() + " 3. Firstname: " + wait.getF()
                + " 4. Lastname: " + wait.getL() + " 5. Weight: " + wait.getW() + " 6. Height: " + wait.getH());
        System.out.println("(0) back / (Y)save");
        System.err.println("Username can't change!");
    }

    public static void checkEditSave() {
        for (User a : arr) {
            if (a.getUser().equals(wait.user)) {
                a.setPass(wait.getPass());
                a.setF(wait.getF());
                a.setL(wait.getL());
                a.setW(wait.getW());
                a.setH(wait.getH());
            }
        }
        showSuccess();
        sayMenu();
    }

    public static void chooseEditWeight() {
        boolean check = false;
        while (!check) {
            try {
                System.out.print("weight : ");
                editWeight = kb.nextDouble();
                if (editWeight == 0) {
                    sayEdit();
                    break;
                }
                else if (editWeight < 0) {
                    showErrorWeightMinimum();
                    chooseEditWeight();
                    break;
                }
                wait.setW(editWeight);
                check = true;
            } catch (InputMismatchException e) {
                showErrorWeight();
                kb.next();
            }
        }
        System.out.println("Weight can use.");
    }

    public static void chooseEditHeight() {
        boolean check = false;
        while (!check) {
            try {
                System.out.print("height : ");
                editHeight = kb.nextDouble();
                if (editHeight == 0) {
                    sayEdit();
                    break;
                }
                else if (editHeight < 0) {
                    showErrorHeightMinimum();
                    chooseEditHeight();
                    break;
                }
                wait.setH(editHeight);
                check = true;
            } catch (InputMismatchException e) {
                showErrorHeight();
                kb.next();
            }
        }
        System.out.println("Height can use.");
    }

    public static void chooseEditPass() {
        System.out.print("password : ");
        editPass = kb.next();
        if (editPass.equals("0")) {
            sayEdit();
        }
        wait.setPass(editPass);
    }

    public static void chooseEditFname() {
        System.out.print("firstname : ");
        editFname = kb.next();
        if (editFname.equals("0")) {
            sayEdit();
        }
        wait.setF(editFname);
    }

    public static void chooseEditLname() {
        System.out.print("lastname : ");
        editLname = kb.next();
        if (editLname.equals("0")) {
            sayEdit();
        }
        wait.setL(editLname);
    }

    // for logout
    public static void logOut() {
        System.out.println("Do you want to logout?(Y/N)");
        showPleaseChoose();
        String input = kb.next();
        if (input.equalsIgnoreCase("Y")) {
            sayLogin();
        } else if (input.equalsIgnoreCase("N")) {
            sayMenu();
        } else {
            logOut();
        }

    }
}

class User {

    String user;
    String pass;
    String fname;
    String lname;
    double weight;
    double height;

    public User(String user, String pass, String fname, String lname, double weight, double height) {
        this.user = user;
        this.pass = pass;
        this.fname = fname;
        this.lname = lname;
        this.weight = weight;
        this.height = height;
    }

    public String toString() {
        String p1 = "Username : " + user;
        String p2 = "\nPassword : " + pass;
        String p3 = "\nFirstname : " + fname;
        String p4 = "\nLastname : " + lname;
        String p5 = "\nWeight : " + Double.toString(weight);
        String p6 = "\nHeight : " + Double.toString(height);
        return p1 + p3 + p4 + p5 + p6;

    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getF() {
        return fname;
    }

    public String getL() {
        return lname;
    }

    public Double getW() {
        return weight;
    }

    public Double getH() {
        return height;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setF(String fname) {
        this.fname = fname;
    }

    public void setL(String lname) {
        this.lname = lname;

    }

    public void setW(double weight) {
        this.weight = weight;
    }

    public void setH(double height) {
        this.height = height;
    }
}
